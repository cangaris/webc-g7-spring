package pl.webc.webcg7spring.common.service;

import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.webc.webcg7spring.category.data.Category;
import pl.webc.webcg7spring.category.service.CategoryService;
import pl.webc.webcg7spring.product.data.Product;
import pl.webc.webcg7spring.product.service.ProductService;

@ExtendWith(MockitoExtension.class)
class CategoryProductServiceImplTest {

    @InjectMocks
    CategoryProductServiceImpl categoryProductService;
    @Mock
    ProductService productService;
    @Mock
    CategoryService categoryService;

    @Test
    void assignCategoryProduct() {
        // given
        Integer categoryId = 3;
        Integer productId = 5;
        Category category = Category.builder()
            .products(new ArrayList<>())
            .build();
        Product product = new Product();
        // when
        Mockito.when(categoryService.findCategory(categoryId)).thenReturn(category);
        Mockito.when(productService.findProduct(productId)).thenReturn(product);
        categoryProductService.assignCategoryProduct(categoryId, productId);
        // then
        Mockito.verify(categoryService).saveCategory(category);
        Assertions.assertEquals(1, category.getProducts().size());
    }
}
