package pl.webc.webcg7spring.user.service;

import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import pl.webc.webcg7spring.user.data.User;
import pl.webc.webcg7spring.user.data.UserRepository;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl service;
    @Mock
    UserRepository repository;

    @Test
    void getAllUsers() {
        // given
        Pageable pageable = PageRequest.of(0, 20);
        List<User> usersList = List.of(User.builder().id(9).build());
        Page<User> usersPage = new PageImpl<>(usersList);
        // when
        Mockito.when(repository.findAll(pageable)).thenReturn(usersPage);
        Page<User> res = service.getAllUsers(pageable);
        // then
        Assertions.assertEquals(9, res.getContent().getFirst().getId());
        Mockito.verify(repository).findAll(pageable);
    }

    @Test
    void saveUser() {
    }

    @Test
    void deleteUser() {
    }

    @Test
    void findUserWhenUserNotFound() {
    }

    @Test
    void findUserWhenUserFound() {
    }

    @Test
    void checkConflictForInsertWhenNameDoesNotExist() {
    }

    @Test
    void checkConflictForInsertWhenNameExists() {
    }

    @Test
    void checkConflictForUpdateWhenNameExists() {
    }

    @Test
    void checkConflictForUpdateWhenNameDoesNotExist() {
    }
}
