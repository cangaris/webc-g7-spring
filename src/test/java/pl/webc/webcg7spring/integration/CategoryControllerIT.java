package pl.webc.webcg7spring.integration;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CategoryControllerIT {

    @Autowired
    MockMvc mockMvc;

    private static final String MANAGER = "MANAGER";

    @WithMockUser(roles = {MANAGER})
    @Order(1)
    @Test
    void saveFirstCategory() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/category")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(CATEGORY_INSERT))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(CATEGORY_INSERTED));
    }

    @WithMockUser(roles = {MANAGER})
    @Order(2)
    @Test
    void saveSecondCategory() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/category")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(CATEGORY_SECOND_INSERT))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(CATEGORY_SECOND_INSERTED));
    }

    @WithMockUser(roles = {MANAGER})
    @Order(3)
    @Test
    void saveCategoryForConflictedName() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/category")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(CATEGORY_INSERT))
            .andExpect(MockMvcResultMatchers.status().isConflict())
            .andExpect(MockMvcResultMatchers.content().json(CATEGORY_CONFLICT));
    }

    @WithMockUser(roles = {MANAGER})
    @Order(4)
    @Test
    void updateCategoryForConflictedName() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.put("/category/2")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(CATEGORY_INSERT))
            .andExpect(MockMvcResultMatchers.status().isConflict())
            .andExpect(MockMvcResultMatchers.content().json(CATEGORY_CONFLICT));
    }

    @WithMockUser(roles = {MANAGER})
    @Order(5)
    @Test
    void saveProduct() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/product")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(PRODUCT_INSERT))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(PRODUCT_INSERTED));
    }

    @WithMockUser(roles = {MANAGER})
    @Order(6)
    @Test
    void assignProductToCategory() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/category/1/product/1"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().string(""));
    }

    @Order(7)
    @Test
    void getAllCategories() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/category"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(ALL_CATEGORIES));
    }

    @WithMockUser(roles = {MANAGER})
    @Order(8)
    @Test
    void updateCategory() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.put("/category/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(CATEGORY_UPDATE))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().string(""));
    }

    @Order(9)
    @Test
    void getCategoryById() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/category/1"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(CATEGORY_UPDATED));
    }

    @WithMockUser(roles = {MANAGER})
    @Order(10)
    @Test
    void assignProductToCategoryWhenProductNotFound() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/category/1/product/2"))
            .andExpect(MockMvcResultMatchers.status().isNotFound())
            .andExpect(MockMvcResultMatchers.content().json(PRODUCT_NOT_FOUND_ID2));
    }

    @WithMockUser(roles = {MANAGER})
    @Order(11)
    @Test
    void assignProductToCategoryWhenCategoryNotFound() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/category/3/product/1"))
            .andExpect(MockMvcResultMatchers.status().isNotFound())
            .andExpect(MockMvcResultMatchers.content().json(CATEGORY_NOT_FOUND_ID3));
    }

    @WithMockUser(roles = {MANAGER})
    @Order(12)
    @Test
    void deleteCategoryById() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/category/1"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().string(""));
    }

    @Order(13)
    @Test
    void getCategoryByIdForDeletedElement() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/category/1"))
            .andExpect(MockMvcResultMatchers.status().isNotFound())
            .andExpect(MockMvcResultMatchers.content().json(CATEGORY_NOT_FOUND_ID1));
    }

    @WithMockUser(roles = {MANAGER})
    @Order(14)
    @Test
    void saveCategoryInvalidPayload() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/category")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(CATEGORY_INVALID_INSERT))
            .andExpect(MockMvcResultMatchers.status().isBadRequest())
            .andExpect(MockMvcResultMatchers.content().json(CATEGORY_BAD_REQ_INSERT));
    }

    @WithMockUser(roles = {MANAGER})
    @Order(15)
    @Test
    void updateCategoryInvalidPayload() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.put("/category/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(CATEGORY_INVALID_UPDATE))
            .andExpect(MockMvcResultMatchers.status().isBadRequest())
            .andExpect(MockMvcResultMatchers.content().json(CATEGORY_BAD_REQ_UPDATE));
    }

    @WithMockUser(roles = {MANAGER})
    @Order(16)
    @Test
    void deleteCategoryByIdForDeletedItem() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/category/1"))
            .andExpect(MockMvcResultMatchers.status().isNotFound())
            .andExpect(MockMvcResultMatchers.content().json(CATEGORY_NOT_FOUND_ID1));
    }

    String ALL_CATEGORIES = """
         [
           {
              "id":1,
              "name":"iPhone",
              "content":"najlepsze iphony na rynku",
              "products":[
                 {
                    "id":1,
                    "name":"iPhone 14"
                 }
              ],
              "image":{
                 "id":1,
                 "uri":"https://wp.pl"
              }
           },
           {
              "id":2,
              "name":"Serwery",
              "content":"najlepsze serwery na rynku",
              "products":[],
              "image":{
                 "id":2,
                 "uri":"https://wp.pl"
              }
           }
        ]
        """;

    String CATEGORY_INSERT = """
        {
            "name": "iPhone",
            "content": "najlepsze iphony na rynku",
            "image": {
                "uri": "https://wp.pl"
            }
        }
        """;

    String CATEGORY_UPDATE = """
        {
            "name": "Macbook",
            "content": "najlepsze macbooki na rynku",
            "image": {
                "uri": "https://wp.pl"
            }
        }
        """;

    String CATEGORY_SECOND_INSERT = """
         {
            "name": "Serwery",
            "content": "najlepsze serwery na rynku",
            "image": {
                "uri": "https://wp.pl"
            }
        }
        """;

    String CATEGORY_UPDATED = """
        {
            "id": 1,
            "name": "Macbook",
            "content": "najlepsze macbooki na rynku",
            "products": [
                {
                    "id": 1,
                    "name": "iPhone 14"
                }
            ],
            "image": {
                "id": 1,
                "uri": "https://wp.pl"
            }
        }
        """;

    String CATEGORY_INSERTED = """
        {
            "id": 1
        }
        """;

    String CATEGORY_SECOND_INSERTED = """
        {
            "id": 2
        }
        """;

    String CATEGORY_NOT_FOUND_ID1 = """
         ["Item not found for category with id 1"]
        """;

    String PRODUCT_NOT_FOUND_ID2 = """
         ["Item not found for product with id 2"]
        """;

    String CATEGORY_NOT_FOUND_ID3 = """
         ["Item not found for category with id 3"]
        """;

    String PRODUCT_INSERTED = """
        {
            "id": 1
        }
        """;

    String PRODUCT_INSERT = """
        {
           "name": "iPhone 14",
           "content": "najlepszy iphone na rynku, polecam!",
           "price": 4367,
           "images": [
               {
                   "uri": "https://onet3.pl"
               },
               {
                   "uri": "https://wp3.pl"
               }
           ]
        }
        """;

    String CATEGORY_INVALID_INSERT = """
        {
         "name": "",
         "content": "najlepsze macbooki na rynku",
         "image": {
             "uri": "https://wp.pl"
         }
        }
        """;

    String CATEGORY_BAD_REQ_INSERT = """
        [
            "Name cannot be blank",
            "Name must be between 5-100"
        ]
        """;

    String CATEGORY_INVALID_UPDATE = """
        {
         "name": "Apple Watches",
         "content": "",
         "image": {
             "uri": "https://wp.pl"
         }
        }
        """;

    String CATEGORY_BAD_REQ_UPDATE = """
        [
            "Content cannot be blank",
            "Content must be between 5-65535"
        ]
        """;

    String CATEGORY_CONFLICT = """
        ["Item is in conflict for category with name iPhone already exists"]
        """;
}
