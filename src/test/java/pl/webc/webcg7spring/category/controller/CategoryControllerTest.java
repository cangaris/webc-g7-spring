package pl.webc.webcg7spring.category.controller;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.webc.webcg7spring.category.data.Category;
import pl.webc.webcg7spring.category.service.CategoryService;
import pl.webc.webcg7spring.common.dtos.DatabaseIdDto;
import pl.webc.webcg7spring.common.dtos.ImageSaveDto;
import pl.webc.webcg7spring.common.service.CategoryProductService;
import pl.webc.webcg7spring.image.data.Image;

@ExtendWith(MockitoExtension.class)
class CategoryControllerTest {

    @InjectMocks
    CategoryController controller;
    @Mock
    CategoryService categoryService;
    @Mock
    CategoryProductService categoryProductService;

    @Test
    void getCategories() {
        // given
        Category category = Category.builder()
            .id(3)
            .products(new ArrayList<>())
            .image(Image.builder().build())
            .build();
        // when
        Mockito.when(categoryService.findAllCategories()).thenReturn(List.of(category));
        List<CategoryDto> res = controller.getCategories();
        // then
        Assertions.assertEquals(1, res.size());
        Assertions.assertEquals(3, res.getFirst().id());
        Mockito.verify(categoryService).findAllCategories();
    }

    @Test
    void getCategory() {
        // given
        Integer categoryId = 1;
        Category category = Category.builder()
            .id(categoryId)
            .products(new ArrayList<>())
            .image(Image.builder().build())
            .build();
        // when
        Mockito.when(categoryService.findCategory(categoryId)).thenReturn(category);
        CategoryDto res = controller.getCategory(categoryId);
        // then
        Mockito.verify(categoryService).findCategory(categoryId);
        Assertions.assertEquals(categoryId, res.id());
    }

    @Test
    void assignCategoryToProduct() {
        // given
        Integer categoryId = 4;
        Integer productId = 8;
        // when
        controller.assignCategoryToProduct(categoryId, productId);
        // then
        Mockito.verify(categoryProductService).assignCategoryProduct(categoryId, productId);
    }

    @Test
    void addCategory() {
        // given
        Category categoryToSave = Category.builder()
            .image(Image.builder().build())
            .build();
        Category categorySaved = Category.builder()
            .id(7)
            .image(Image.builder().build())
            .build();
        CategoryInsertDto categoryInsertDto = CategoryInsertDto.builder()
            .name("name")
            .image(ImageSaveDto.builder().build())
            .build();
        // when
        Mockito.when(categoryService.saveCategory(categoryToSave)).thenReturn(categorySaved);
        DatabaseIdDto res = controller.addCategory(categoryInsertDto);
        // then
        Mockito.verify(categoryService).checkConflict(categoryInsertDto.name());
        Mockito.verify(categoryService).saveCategory(categoryToSave);
        Assertions.assertEquals(categorySaved.getId(), res.getId());
    }

    @Test
    void updateCategory() {
        // given
        Integer categoryId = 1;
        Category category = Category.builder()
            .image(Image.builder().build())
            .build();
        CategoryUpdateDto categoryUpdateDto = CategoryUpdateDto.builder()
            .name("name")
            .image(ImageSaveDto.builder().build())
            .build();
        // when
        Mockito.when(categoryService.findCategory(categoryId)).thenReturn(category);
        controller.updateCategory(categoryId, categoryUpdateDto);
        // then
        Mockito.verify(categoryService).checkConflict(categoryUpdateDto.name(), categoryId);
        Mockito.verify(categoryService).findCategory(categoryId);
        Mockito.verify(categoryService).saveCategory(category);
    }

    @Test
    void deleteCategory() {
        // given
        Integer categoryId = 1;
        Category category = Category.builder()
            .id(categoryId)
            .build();
        // when
        Mockito.when(categoryService.findCategory(categoryId)).thenReturn(category);
        controller.deleteCategory(categoryId);
        // then
        Mockito.verify(categoryService).findCategory(categoryId);
        Mockito.verify(categoryService).deleteCategory(categoryId);
    }
}
