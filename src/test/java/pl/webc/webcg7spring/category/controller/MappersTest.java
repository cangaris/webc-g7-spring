package pl.webc.webcg7spring.category.controller;

import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.webc.webcg7spring.category.data.Category;
import pl.webc.webcg7spring.common.dtos.ImageSaveDto;
import pl.webc.webcg7spring.image.data.Image;
import pl.webc.webcg7spring.product.data.Product;

class MappersTest {

    @Test
    void mapFromCategoryToCategoryDto() {
        // given
        Product product = Product.builder()
            .id(3)
            .name("test")
            .build();
        List<Product> products = List.of(product);
        Image image = Image.builder()
            .id(2)
            .uri("https://onet.pl")
            .build();
        Category category = Category.builder()
            .id(1)
            .name("test")
            .content("content")
            .products(products)
            .image(image)
            .build();
        // when
        CategoryDto categoryDto = Mappers.map(category);
        // then
        Assertions.assertEquals(category.getId(), categoryDto.id());
        Assertions.assertEquals(category.getName(), categoryDto.name());
        Assertions.assertEquals(category.getContent(), categoryDto.content());
        Assertions.assertEquals(category.getImage().getId(), categoryDto.image().id());
        Assertions.assertEquals(category.getImage().getUri(), categoryDto.image().uri());
        Assertions.assertEquals(category.getProducts().getFirst().getId(), categoryDto.products().getFirst().id());
        Assertions.assertEquals(category.getProducts().getFirst().getName(), categoryDto.products().getFirst().name());
    }

    @Test
    void mapFromCategoryInsertDtoToCategory() {
        // given
        CategoryInsertDto categoryInsertDto = CategoryInsertDto.builder()
            .name("test")
            .content("test test test")
            .image(ImageSaveDto.builder()
                .uri("https://onet.pl")
                .build())
            .build();
        // when
        Category category = Mappers.map(categoryInsertDto);
        // then
        Assertions.assertEquals(categoryInsertDto.name(), category.getName());
        Assertions.assertEquals(categoryInsertDto.content(), category.getContent());
        Assertions.assertEquals(categoryInsertDto.image().uri(), category.getImage().getUri());
    }

    @Test
    void mapFromCategoryUpdateDtoToCategory() {
        // given
        Category category = Category.builder()
            .image(Image.builder().build())
            .build();
        CategoryUpdateDto categoryUpdateDto = CategoryUpdateDto.builder()
            .name("test")
            .content("test test test")
            .image(ImageSaveDto.builder()
                .uri("https://onet.pl")
                .build())
            .build();
        // when
        Mappers.map(category, categoryUpdateDto);
        // then
        Assertions.assertEquals(category.getName(), categoryUpdateDto.name());
        Assertions.assertEquals(category.getContent(), categoryUpdateDto.content());
        Assertions.assertEquals(category.getImage().getUri(), categoryUpdateDto.image().uri());
    }

    @Test
    void fromProductToProductDto() {
        // given
        Product product = Product.builder()
            .id(2)
            .name("test")
            .build();
        // when
        CategoryDto.ProductDto res = Mappers.map(product);
        // then
        Assertions.assertEquals(product.getId(), res.id());
        Assertions.assertEquals(product.getName(), res.name());
    }
}
