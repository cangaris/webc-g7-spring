package pl.webc.webcg7spring.category.service;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.webc.webcg7spring.category.data.Category;
import pl.webc.webcg7spring.category.data.CategoryRepository;
import pl.webc.webcg7spring.common.exceptions.ConflictException;
import pl.webc.webcg7spring.common.exceptions.NotFoundException;

@ExtendWith(MockitoExtension.class)
class CategoryServiceImplTest {

    @InjectMocks
    CategoryServiceImpl categoryService;

    @Mock
    CategoryRepository repository;

    @Test
    void findAllCategories() {
        // given
        List<Category> categoriesFromRepo = List.of(Category.builder().id(3).build());
        // when
        Mockito.when(repository.findAll()).thenReturn(categoriesFromRepo);
        List<Category> categoriesFromService = categoryService.findAllCategories();
        // then
        Assertions.assertEquals(3, categoriesFromService.getFirst().getId());
        Mockito.verify(repository).findAll();
    }

    @Test
    void findCategoryWhenCategoryNotFound() {
        // given
        Integer id = 5;
        // when
        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());
        // then
        Assertions.assertThrows(NotFoundException.class, () -> {
            categoryService.findCategory(id);
        });
        Mockito.verify(repository).findById(id);
    }

    @Test
    void findCategoryWhenCategoryFound() {
        // given
        Integer id = 5;
        Category categoryFromRepo = Category.builder().id(id).build();
        // when
        Mockito.when(repository.findById(id)).thenReturn(Optional.of(categoryFromRepo));
        Category categoryFromService = categoryService.findCategory(id);
        // then
        Mockito.verify(repository).findById(id);
        Assertions.assertEquals(5, categoryFromService.getId());
    }

    /**
     * given - parametry wejściowe testu
     * when - co się musi wydarzyć aby test przeszedł założoną ścieżką
     * then - jakie warunki muszą być spełnione aby zaliczyć test
     */
    @Test
    void saveCategory() {
        // given
        Category categoryToSave = Category.builder().build();
        Category categoryAfterSave = Category.builder().id(1).build();
        // when
        Mockito.when(repository.save(categoryToSave)).thenReturn(categoryAfterSave);
        Category savedCategory = categoryService.saveCategory(categoryToSave);
        // then
        Mockito.verify(repository).save(categoryToSave); // czy save na repo sie wykonał?
        Assertions.assertEquals(1, savedCategory.getId());
    }

    @Test
    void deleteCategory() {
        // given
        Integer categoryId = 1;
        // when
        categoryService.deleteCategory(categoryId);
        // then
        Mockito.verify(repository).deleteById(categoryId);
    }

    @Test
    void checkConflictForInsertWhenNameExists() {
        // given
        String name = "name";
        // when
        Mockito.when(repository.existsByName(name)).thenReturn(true);
        // then
        Assertions.assertThrows(ConflictException.class, () -> {
            categoryService.checkConflict(name);
        });
        Mockito.verify(repository).existsByName(name);
    }

    @Test
    void checkConflictForInsertWhenNameDoesNotExist() {
        // given
        String name = "name";
        // when
        Mockito.when(repository.existsByName(name)).thenReturn(false);
        categoryService.checkConflict(name);
        // then
        Mockito.verify(repository).existsByName(name);
    }

    @Test
    void checkConflictForUpdateWhenNameExists() {
        // given
        Integer categoryId = 1;
        String name = "name";
        // when
        Mockito.when(repository.existsByNameAndIdNot(name, categoryId)).thenReturn(true);
        // then
        Assertions.assertThrows(ConflictException.class, () -> {
            categoryService.checkConflict(name, categoryId);
        });
        Mockito.verify(repository).existsByNameAndIdNot(name, categoryId);
    }

    @Test
    void checkConflictForUpdateWhenNameDoesNotExist() {
        // given
        Integer categoryId = 1;
        String name = "name";
        // when
        Mockito.when(repository.existsByNameAndIdNot(name, categoryId)).thenReturn(false);
        categoryService.checkConflict(name, categoryId);
        // then
        Mockito.verify(repository).existsByNameAndIdNot(name, categoryId);
    }
}
