CREATE TABLE product
(
    id      INT AUTO_INCREMENT NOT NULL,
    name    VARCHAR(200)       NOT NULL,
    content TEXT               NOT NULL,
    price   DECIMAL            NOT NULL,
    CONSTRAINT pk_product PRIMARY KEY (id)
);
