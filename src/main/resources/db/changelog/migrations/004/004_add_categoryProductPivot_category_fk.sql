ALTER TABLE category_product_pivot
    ADD CONSTRAINT fk_catpropiv_on_category FOREIGN KEY (category_id) REFERENCES category (id);
