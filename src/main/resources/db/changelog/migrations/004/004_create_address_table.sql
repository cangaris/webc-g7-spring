CREATE TABLE address
(
    id       INT AUTO_INCREMENT NOT NULL,
    zip_code VARCHAR(10)        NOT NULL,
    street   VARCHAR(30)        NOT NULL,
    city     VARCHAR(30)        NOT NULL,
    user_id  INT                NULL,
    CONSTRAINT pk_address PRIMARY KEY (id)
);
