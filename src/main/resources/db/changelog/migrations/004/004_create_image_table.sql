CREATE TABLE image
(
    id         INT AUTO_INCREMENT NOT NULL,
    uri        VARCHAR(255)       NULL,
    product_id INT                NULL,
    CONSTRAINT pk_image PRIMARY KEY (id)
);
