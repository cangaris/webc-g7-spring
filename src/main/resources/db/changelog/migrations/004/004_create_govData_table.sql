CREATE TABLE gov_data
(
    id              INT AUTO_INCREMENT NOT NULL,
    personal_number VARCHAR(20)        NOT NULL,
    vat_number      VARCHAR(20)        NOT NULL,
    CONSTRAINT pk_govdata PRIMARY KEY (id)
);
