CREATE TABLE category
(
    id      INT AUTO_INCREMENT NOT NULL,
    name    VARCHAR(100)       NOT NULL,
    content TEXT               NOT NULL,
    CONSTRAINT pk_category PRIMARY KEY (id)
);
