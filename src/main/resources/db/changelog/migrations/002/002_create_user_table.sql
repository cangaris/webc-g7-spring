CREATE TABLE app_user
(
    id        INT AUTO_INCREMENT NOT NULL,
    firstname VARCHAR(50)        NOT NULL,
    lastname  VARCHAR(50)        NOT NULL,
    email     VARCHAR(100)       NOT NULL,
    password  VARCHAR(255)       NOT NULL,
    CONSTRAINT pk_app_user PRIMARY KEY (id)
);
