package pl.webc.webcg7spring.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
class LoggerAspect {

    @Around("@annotation(Logger)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("Around START");
        var arg = joinPoint.getArgs();
        var object = joinPoint.proceed(arg);
        System.out.println("Around STOP");
        return object;
    }

    // @Before("@annotation(Logger)")
    public void before(JoinPoint joinPoint) {
        var data = joinPoint.getArgs()[0];
        System.out.println("Before");
        System.out.println(data);
    }

    // @After("@annotation(Logger)")
    public void after() {
        System.out.println("After");
    }
}
