package pl.webc.webcg7spring.common.advices;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.webc.webcg7spring.common.exceptions.ConflictException;
import pl.webc.webcg7spring.common.exceptions.NotFoundException;

@RestControllerAdvice
class CustomControllerAdvice {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public List<String> handleMethodArgumentNotValidException(
        MethodArgumentNotValidException exception) {
        return exception.getAllErrors().stream()
            .map(objectError -> objectError.getDefaultMessage())
            .toList();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public List<String> handleNotFoundException(NotFoundException exception) {
        return List.of("Item not found for " + exception.getMessage());
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(ConflictException.class)
    public List<String> handleConflictException(ConflictException exception) {
        return List.of("Item is in conflict for " + exception.getMessage());
    }
}
