package pl.webc.webcg7spring.common.service;

public interface CategoryProductService {
    void assignCategoryProduct(Integer categoryId, Integer productId);
}
