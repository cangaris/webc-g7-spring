package pl.webc.webcg7spring.common.service;

import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.webc.webcg7spring.category.data.Category;
import pl.webc.webcg7spring.category.service.CategoryService;
import pl.webc.webcg7spring.product.data.Product;
import pl.webc.webcg7spring.product.service.ProductService;

@RequiredArgsConstructor
@Service
class CategoryProductServiceImpl implements CategoryProductService {

    private final ProductService productService;
    private final CategoryService categoryService;

    @Override
    public void assignCategoryProduct(Integer categoryId, Integer productId) {
        Category category = categoryService.findCategory(categoryId);
        Product product = productService.findProduct(productId);
        boolean productNotAssignedToCategory = category.getProducts().stream()
            .noneMatch(p -> Objects.equals(p.getId(), productId));
        if (productNotAssignedToCategory) {
            category.getProducts().add(product);
            // product.getCategories().add(category);
            // productService.saveProduct(product); tak nie można bo zapisujemy z parenta relacji a nie child
            categoryService.saveCategory(category);
        }
    }
}
