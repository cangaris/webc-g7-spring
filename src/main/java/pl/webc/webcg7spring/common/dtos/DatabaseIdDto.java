package pl.webc.webcg7spring.common.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class DatabaseIdDto {
    private Integer id;
}
