package pl.webc.webcg7spring.common.dtos;

import lombok.Builder;

@Builder
public record ImageDto(
    Integer id,
    String uri
) {
}
