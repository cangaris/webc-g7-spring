package pl.webc.webcg7spring.common.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Builder;

@Builder
public record ImageSaveDto(
    @Size(message = "Uri must be between 10-255", min = 10, max = 255)
    @NotBlank(message = "Uri cannot be blank")
    String uri
) {
}
