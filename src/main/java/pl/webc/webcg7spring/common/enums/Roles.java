package pl.webc.webcg7spring.common.enums;

public enum Roles {
    ADMIN,
    MANAGER,
    CLIENT
}
