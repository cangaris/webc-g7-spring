package pl.webc.webcg7spring.common.exceptions;

public class FileException extends RuntimeException {
    public FileException(String message) {
        super(message);
    }
}
