package pl.webc.webcg7spring.user.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.webc.webcg7spring.user.data.User;

public interface UserService {

    Page<User> getAllUsers(Pageable pageable);

    User insertUser(User user);

    User updateUser(User user);

    void deleteUser(Integer id);

    User findUser(Integer needleId);

    User findUser(String email);

    void checkConflict(String email);

    void checkConflict(String email, Integer id);
}
