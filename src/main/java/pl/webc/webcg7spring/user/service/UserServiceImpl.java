package pl.webc.webcg7spring.user.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.webc.webcg7spring.common.exceptions.ConflictException;
import pl.webc.webcg7spring.common.exceptions.NotFoundException;
import pl.webc.webcg7spring.user.data.User;
import pl.webc.webcg7spring.user.data.UserRepository;

@Service
@RequiredArgsConstructor
class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Page<User> getAllUsers(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public User insertUser(User user) {
        String encoded = passwordEncoder.encode(user.getPassword());
        user.setPassword(encoded);
        return repository.save(user);
    }

    @Override
    public User updateUser(User user) {
        return repository.save(user);
    }

    @Override
    public void deleteUser(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public User findUser(Integer needleId) {
        return repository.findById(needleId)
            .orElseThrow(() -> new NotFoundException("user with id " + needleId));
    }

    @Override
    public User findUser(String email) {
        return repository.findByEmail(email)
            .orElseThrow(() -> new NotFoundException("user with email " + email));
    }

    @Override
    public void checkConflict(String email) {
        boolean isConflict = repository.existsByEmail(email);
        if (isConflict) {
            throw new ConflictException("user with name: " + email + " already exists");
        }
    }

    @Override
    public void checkConflict(String email, Integer id) {
        boolean isConflict = repository.existsByEmailAndIdNot(email, id);
        if (isConflict) {
            throw new ConflictException("user with email: " + email + " already exists");
        }
    }
}
