package pl.webc.webcg7spring.user.controller;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.webc.webcg7spring.common.enums.Roles;

@Builder
@Getter
@Setter
class UserDto {
    private Integer id;
    private String firstname;
    private String lastname;
    private String email;
    private Roles role;
    private String vatNumber;
    private List<AddressDto> address;

    @Builder
    @Getter
    @Setter
    static class AddressDto {
        private String zipCode;
        private String street;
        private String city;
    }
}
