package pl.webc.webcg7spring.user.controller;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import pl.webc.webcg7spring.common.enums.Roles;

@Getter
@Setter
class UserUpdateDto {
    @NotBlank(message = "Firstname cannot be blank")
    @Size(min = 3, max = 255, message = "Firstname must have 3-255 chars")
    private String firstname;

    @NotBlank(message = "Lastname cannot be blank")
    @Size(min = 3, max = 255, message = "Lastname must have 3-255 chars")
    private String lastname;

    @NotBlank(message = "Email cannot be blank")
    @Pattern(regexp = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$", message = "Email pattern is invalid")
    private String email;

    @NotNull(message = "Role cannot be null")
    private Roles role;

    @NotBlank(message = "Personal number cannot be blank")
    @Size(min = 10, max = 20, message = "Personal number must have 10-20 chars")
    private String personalNumber;

    @NotBlank(message = "VAT number cannot be blank")
    @Size(min = 10, max = 20, message = "VAT number must have 10-20 chars")
    private String vatNumber;

    private List<@Valid AddressUpdateDto> address;

    @Getter
    @Setter
    static class AddressUpdateDto {
        @NotBlank(message = "Zip code cannot be blank")
        @Size(min = 3, max = 10, message = "Zip code must have 3-10 chars")
        private String zipCode;

        @NotBlank(message = "Street cannot be blank")
        @Size(min = 3, max = 30, message = "Street must have 3-30 chars")
        private String street;

        @NotBlank(message = "City cannot be blank")
        @Size(min = 3, max = 30, message = "City must have 3-30 chars")
        private String city;
    }
}
