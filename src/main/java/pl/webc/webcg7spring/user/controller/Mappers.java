package pl.webc.webcg7spring.user.controller;

import java.util.List;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;
import pl.webc.webcg7spring.user.data.Address;
import pl.webc.webcg7spring.user.data.GovData;
import pl.webc.webcg7spring.user.data.User;

@UtilityClass
class Mappers {

    UserDto map(User user) {
        List<UserDto.AddressDto> address = user.getAddress().stream()
            .map(addr -> map(addr))
            .toList();
        return UserDto.builder()
            .id(user.getId())
            .firstname(user.getFirstname())
            .lastname(user.getLastname())
            .email(user.getEmail())
            .role(user.getRole())
            .vatNumber(user.getGovData().getVatNumber())
            .address(address)
            .build();
    }

    User map(UserInsertDto userInsertDto) {
        List<Address> address = userInsertDto.getAddress().stream()
            .map(addressInsertDto -> map(addressInsertDto))
            .collect(Collectors.toList()); // nie może być toList bo jest niemodyfikowalny
        GovData govData = GovData.builder()
            .personalNumber(userInsertDto.getPersonalNumber())
            .vatNumber(userInsertDto.getVatNumber())
            .build();
        return User.builder()
            .firstname(userInsertDto.getFirstname())
            .lastname(userInsertDto.getLastname())
            .email(userInsertDto.getEmail())
            .password(userInsertDto.getPassword())
            .role(userInsertDto.getRole())
            .govData(govData)
            .address(address)
            .build();
    }

    void map(User user, UserUpdateDto userUpdateDto) {
        user.setFirstname(userUpdateDto.getFirstname());
        user.setLastname(userUpdateDto.getLastname());
        user.setEmail(userUpdateDto.getEmail());
        user.setRole(userUpdateDto.getRole());

        GovData govData = user.getGovData();
        govData.setVatNumber(userUpdateDto.getVatNumber());
        govData.setPersonalNumber(userUpdateDto.getPersonalNumber());

        List<Address> address = userUpdateDto.getAddress().stream()
            .map(addressUpdateDto -> map(addressUpdateDto))
            .collect(Collectors.toList()); // nie może być toList bo jest niemodyfikowalny
        user.getAddress().clear();
        user.getAddress().addAll(address);
    }

    Address map(UserInsertDto.AddressInsertDto addressInsertDto) {
        return Address.builder()
            .zipCode(addressInsertDto.getZipCode())
            .street(addressInsertDto.getStreet())
            .city(addressInsertDto.getCity())
            .build();
    }

    UserDto.AddressDto map(Address address) {
        return UserDto.AddressDto.builder()
            .city(address.getCity())
            .street(address.getStreet())
            .zipCode(address.getZipCode())
            .build();
    }

    Address map(UserUpdateDto.AddressUpdateDto addressUpdateDto) {
        return Address.builder()
            .zipCode(addressUpdateDto.getZipCode())
            .street(addressUpdateDto.getStreet())
            .city(addressUpdateDto.getCity())
            .build();
    }
}
