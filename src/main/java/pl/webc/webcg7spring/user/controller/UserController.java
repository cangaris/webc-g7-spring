package pl.webc.webcg7spring.user.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.webc.webcg7spring.common.dtos.DatabaseIdDto;
import pl.webc.webcg7spring.user.data.User;
import pl.webc.webcg7spring.user.service.UserService;

// @Controller // wybieramy gdy chcemy mieć frontend w ramach projektu spring (MVC)
// CRUD - Create Read Update Delete
@RestController
@RequiredArgsConstructor
class UserController {

    private final UserService service;

    @GetMapping("/user") // http://localhost:8080/user ? page = 0 & size = 20
    public Page<UserDto> getUsers(Pageable pageable) {
        return service.getAllUsers(pageable).map(user -> Mappers.map(user));
    }

    @GetMapping("/user/self") // http://localhost:8080/user/self
    public UserDto getUser(Authentication authentication) { // Authentication lub Principal
        String email = authentication.getName(); // username zalogowanego użytkownika
        User user = service.findUser(email);
        return Mappers.map(user);
    }

    @GetMapping("/user/{id}") // http://localhost:8080/user/4
    public UserDto getUser(@PathVariable Integer id) {
        User user = service.findUser(id);
        return Mappers.map(user);
    }

    @PostMapping("/user")
    public DatabaseIdDto addUser(@RequestBody @Valid UserInsertDto userInsert) {
        service.checkConflict(userInsert.getEmail());
        User user = Mappers.map(userInsert);
        user = service.insertUser(user);
        return new DatabaseIdDto(user.getId());
    }

    // @PatchMapping // fragmenty rekordu (wybrane kolumny)
    @PutMapping("/user/{id}") // update wszystkich kolumn
    public void updateUser(@PathVariable Integer id, @RequestBody @Valid UserUpdateDto userUpdate) {
        service.checkConflict(userUpdate.getEmail(), id);
        User user = service.findUser(id);
        Mappers.map(user, userUpdate);
        service.updateUser(user);
    }

    @DeleteMapping("/user/{id}")
    public void deleteUser(@PathVariable Integer id) {
        User user = service.findUser(id);
        service.deleteUser(user.getId());
    }
}
