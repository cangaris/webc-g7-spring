package pl.webc.webcg7spring.file.controller;

import java.io.IOException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pl.webc.webcg7spring.file.service.FileService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/file")
class FileController {

    private final FileService fileService;

    @GetMapping("/{filename}")
    public ResponseEntity<byte[]> getFile(@PathVariable String filename) throws IOException {
        MediaType mediaType = fileService.getMediaTypeByFilename(filename);
        byte[] data = fileService.getFile(filename);
        return ResponseEntity
            .status(HttpStatus.OK)
            .contentType(mediaType)
            .body(data);
    }

    @PostMapping
    public void upload(@RequestParam("file") MultipartFile multipartFile,
                       @RequestParam("catalog") String catalog) throws IOException {
        fileService.uploadFile(catalog, multipartFile);
    }
}
