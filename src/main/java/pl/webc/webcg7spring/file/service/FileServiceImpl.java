package pl.webc.webcg7spring.file.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.webc.webcg7spring.common.exceptions.FileException;

@RequiredArgsConstructor
@Service
class FileServiceImpl implements FileService {

    @Value("${upload.dest}")
    private String uploadDest;

    @Override
    public void uploadFile(String subCatalog, MultipartFile multipartFile) throws IOException {
        validateImage(multipartFile.getOriginalFilename());
        Path pathToFile = getPathToFile(subCatalog, multipartFile.getOriginalFilename());
        try (InputStream inputStream = multipartFile.getInputStream()) {
            Files.copy(inputStream, pathToFile, StandardCopyOption.REPLACE_EXISTING);
        }
        // todo: zadanie domowe -> zapis do bazy
    }

    @Override
    public byte[] getFile(String filename) throws IOException {
        Path pathToFile = getPathToFile("product", filename);
        return Files.readAllBytes(pathToFile);
    }

    private Path getPathToFile(String subCatalog, String filename) throws IOException {
        Path pathToCatalog = Path.of(uploadDest, subCatalog);
        if (!Files.exists(pathToCatalog)) {
            Files.createDirectories(pathToCatalog);
        }
        return pathToCatalog.resolve(Path.of(filename));
    }

    public void validateImage(String filename) {
        String ext = FilenameUtils.getExtension(filename);
        switch (ext) {
            case "jpg", "jpeg", "png", "gif" -> {
            }
            default -> throw new FileException("Invalid media type");
        }
    }

    public MediaType getMediaTypeByFilename(String filename) {
        String ext = FilenameUtils.getExtension(filename);
        return switch (ext) {
            case "jpg", "jpeg" -> MediaType.IMAGE_JPEG;
            case "png" -> MediaType.IMAGE_PNG;
            case "gif" -> MediaType.IMAGE_GIF;
            default -> throw new FileException("Invalid media type");
        };
    }
}
