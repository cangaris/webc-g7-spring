package pl.webc.webcg7spring.file.service;

import java.io.IOException;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {

    void uploadFile(String subCatalog, MultipartFile multipartFile) throws IOException;

    byte[] getFile(String filename) throws IOException;

    void validateImage(String filename);

    MediaType getMediaTypeByFilename(String filename);
}
