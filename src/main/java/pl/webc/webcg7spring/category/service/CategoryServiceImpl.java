package pl.webc.webcg7spring.category.service;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.webc.webcg7spring.category.data.Category;
import pl.webc.webcg7spring.category.data.CategoryRepository;
import pl.webc.webcg7spring.common.exceptions.ConflictException;
import pl.webc.webcg7spring.common.exceptions.NotFoundException;

@Service
@RequiredArgsConstructor
class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository repository;

    @Override
    public List<Category> findAllCategories() {
        return repository.findAll();
    }

    @Override
    public Category findCategory(Integer needleId) {
        return repository.findById(needleId)
            .orElseThrow(() -> new NotFoundException("category with id " + needleId));
    }

    @Override
    public Category saveCategory(Category category) {
        return repository.save(category);
    }

    @Override
    public void deleteCategory(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public void checkConflict(String name) {
        if (repository.existsByName(name)) {
            throw new ConflictException("category with name " + name + " already exists");
        }
    }

    @Override
    public void checkConflict(String name, Integer id) {
        if (repository.existsByNameAndIdNot(name, id)) {
            throw new ConflictException("category with name " + name + " already exists");
        }
    }
}
