package pl.webc.webcg7spring.category.service;

import java.util.List;
import pl.webc.webcg7spring.category.data.Category;

public interface CategoryService {

    List<Category> findAllCategories();

    Category findCategory(Integer needleId);

    Category saveCategory(Category category);

    void deleteCategory(Integer id);

    void checkConflict(String name);

    void checkConflict(String name, Integer id);
}
