package pl.webc.webcg7spring.category.controller;

import java.util.List;
import lombok.experimental.UtilityClass;
import pl.webc.webcg7spring.category.data.Category;
import pl.webc.webcg7spring.common.dtos.ImageDto;
import pl.webc.webcg7spring.image.data.Image;
import pl.webc.webcg7spring.product.data.Product;

@UtilityClass
class Mappers {

    CategoryDto map(Category category) {
        List<CategoryDto.ProductDto> products = category.getProducts().stream()
            .map(product -> map(product))
            .toList();
        ImageDto image = ImageDto.builder()
            .id(category.getImage().getId())
            .uri(category.getImage().getUri())
            .build();
        return CategoryDto.builder()
            .id(category.getId())
            .name(category.getName())
            .content(category.getContent())
            .products(products)
            .image(image)
            .build();
    }

    Category map(CategoryInsertDto categoryInsertDto) {
        Image image = Image.builder()
            .uri(categoryInsertDto.image().uri())
            .build();
        return Category.builder()
            .name(categoryInsertDto.name())
            .content(categoryInsertDto.content())
            .image(image)
            .build();
    }

    void map(Category category, CategoryUpdateDto categoryUpdateDto) {
        category.setName(categoryUpdateDto.name());
        category.setContent(categoryUpdateDto.content());
        category.getImage().setUri(categoryUpdateDto.image().uri());
    }

    CategoryDto.ProductDto map(Product product) {
        return CategoryDto.ProductDto.builder()
            .id(product.getId())
            .name(product.getName())
            .build();
    }
}
