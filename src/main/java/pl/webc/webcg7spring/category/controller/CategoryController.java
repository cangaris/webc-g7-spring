package pl.webc.webcg7spring.category.controller;

import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.webc.webcg7spring.aop.Logger;
import pl.webc.webcg7spring.category.data.Category;
import pl.webc.webcg7spring.category.service.CategoryService;
import pl.webc.webcg7spring.common.dtos.DatabaseIdDto;
import pl.webc.webcg7spring.common.service.CategoryProductService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/category")
class CategoryController {

    private final CategoryService service;
    private final CategoryProductService categoryProductService;

    @GetMapping
    public List<CategoryDto> getCategories() {
        return service.findAllCategories().stream()
            .map(category -> Mappers.map(category))
            .toList();
    }

    @Logger
    @GetMapping("/{id}")
    public CategoryDto getCategory(@PathVariable Integer id) {
        Category category = service.findCategory(id);
        return Mappers.map(category);
    }

    @PostMapping("/{categoryId}/product/{productId}")
    public void assignCategoryToProduct(@PathVariable Integer categoryId, @PathVariable Integer productId) {
        categoryProductService.assignCategoryProduct(categoryId, productId);
    }

    @PostMapping
    public DatabaseIdDto addCategory(@RequestBody @Valid CategoryInsertDto categoryInsertDto) {
        service.checkConflict(categoryInsertDto.name());
        Category category = Mappers.map(categoryInsertDto);
        category = service.saveCategory(category);
        return new DatabaseIdDto(category.getId());
    }

    @PutMapping("/{id}")
    public void updateCategory(@PathVariable Integer id, @RequestBody @Valid CategoryUpdateDto categoryUpdateDto) {
        service.checkConflict(categoryUpdateDto.name(), id);
        Category category = service.findCategory(id);
        Mappers.map(category, categoryUpdateDto);
        service.saveCategory(category);
    }

    @DeleteMapping("/{id}")
    public void deleteCategory(@PathVariable Integer id) {
        Category category = service.findCategory(id);
        service.deleteCategory(category.getId());
    }
}
