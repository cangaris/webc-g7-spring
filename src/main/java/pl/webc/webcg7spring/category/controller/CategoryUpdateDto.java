package pl.webc.webcg7spring.category.controller;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import pl.webc.webcg7spring.common.dtos.ImageSaveDto;

@Builder
record CategoryUpdateDto(
    @Size(message = "Name must be between 5-100", min = 5, max = 100)
    @NotBlank(message = "Name cannot be blank")
    String name,

    @Size(message = "Content must be between 5-65535", min = 5, max = 65_535)
    @NotBlank(message = "Content cannot be blank")
    String content,

    @Valid
    ImageSaveDto image
) {
}
