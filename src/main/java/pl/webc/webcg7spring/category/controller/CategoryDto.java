package pl.webc.webcg7spring.category.controller;

import java.util.List;
import lombok.Builder;
import pl.webc.webcg7spring.common.dtos.ImageDto;

@Builder
record CategoryDto(
    Integer id,
    String name,
    String content,
    List<ProductDto> products,
    ImageDto image
) {
    @Builder
    record ProductDto(Integer id, String name) {
    }
}
