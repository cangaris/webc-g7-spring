package pl.webc.webcg7spring.schedulers;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
class Scheduler {

    // @Scheduled(initialDelay = 5, fixedDelay = 1, timeUnit = TimeUnit.SECONDS, zone = "Europe/Warsaw")
    @Scheduled(cron = "0 15 10 15 * ?", zone = "Europe/Warsaw")
    public void scheduleFixedDelayTask() {
        System.out.println("Fixed delay task - " + System.currentTimeMillis() / 1000);
    }
}
