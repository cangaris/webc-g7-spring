package pl.webc.webcg7spring.config;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import pl.webc.webcg7spring.common.enums.Roles;
import pl.webc.webcg7spring.user.data.UserRepository;

@RequiredArgsConstructor
@Configuration // tworzy beana typu SecurityConfig
class SecurityConfig {

    private static final String ADMIN = Roles.ADMIN.name();
    private static final String MANAGER = Roles.MANAGER.name();
    private final UserRepository userRepository;

    @Value("${rememberMeKey}")
    private String rememberMeKey;

    @Bean // tworzy bean typu SecurityFilterChain
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity
            .csrf(csrf -> csrf.disable()) // todo?
            .cors(cors -> cors.configurationSource(corsConfigurationSource()))
            .formLogin(form -> form.loginPage("/login")
                .successHandler((req, resp, auth) -> resp.setStatus(HttpStatus.OK.value()))
                .failureHandler((req, resp, ex) -> resp.setStatus(HttpStatus.BAD_REQUEST.value()))
                .permitAll()
            )
            .logout(form -> form.logoutUrl("/logout")
                .logoutSuccessHandler((req, resp, auth) -> resp.setStatus(HttpStatus.OK.value()))
                .permitAll()
            )
            .rememberMe(rememberMeConfigurer -> rememberMeConfigurer
                .key(rememberMeKey)
                .rememberMeParameter("rememberMe")
                .rememberMeCookieName("rememberMe")
                .tokenValiditySeconds(60 * 60 * 24 * 6)
                .useSecureCookie(true)
            )
            .exceptionHandling(handler -> handler
                .authenticationEntryPoint((req, resp, ex) -> resp.setStatus(HttpStatus.UNAUTHORIZED.value()))
            )
            .authorizeHttpRequests(auth ->
                auth
                    // product
                    .requestMatchers(HttpMethod.GET, "/product").permitAll()
                    .requestMatchers(HttpMethod.GET, "/product/{id}").permitAll()
                    .requestMatchers(HttpMethod.POST, "/product").hasAnyRole(ADMIN, MANAGER)
                    .requestMatchers(HttpMethod.PUT, "/product/{id}").hasAnyRole(ADMIN, MANAGER)
                    .requestMatchers(HttpMethod.DELETE, "/product/{id}").hasAnyRole(ADMIN, MANAGER)
                    // category
                    .requestMatchers(HttpMethod.GET, "/category").permitAll()
                    .requestMatchers(HttpMethod.GET, "/category/{id}").permitAll()
                    .requestMatchers(HttpMethod.POST, "/category").hasAnyRole(ADMIN, MANAGER)
                    .requestMatchers(HttpMethod.POST, "/category/{categoryId}/product/{productId}")
                    .hasAnyRole(ADMIN, MANAGER)
                    .requestMatchers(HttpMethod.PUT, "/category/{id}").hasAnyRole(ADMIN, MANAGER)
                    .requestMatchers(HttpMethod.DELETE, "/category/{id}").hasAnyRole(ADMIN, MANAGER)
                    // file
                    .requestMatchers(HttpMethod.GET, "/file/{filename}").permitAll()
                    .requestMatchers(HttpMethod.POST, "/file").hasAnyRole(ADMIN, MANAGER)
                    // user
                    .requestMatchers(HttpMethod.GET, "/user/self").authenticated()
                    .requestMatchers(HttpMethod.GET, "/user").hasAnyRole(ADMIN, MANAGER)
                    .requestMatchers(HttpMethod.GET, "/user/{id}").hasAnyRole(ADMIN, MANAGER)
                    .requestMatchers(HttpMethod.POST, "/user").hasRole(ADMIN)
                    .requestMatchers(HttpMethod.PUT, "/user/{id}").hasRole(ADMIN)
                    .requestMatchers(HttpMethod.DELETE, "/user/{id}").hasRole(ADMIN)
                    // other
                    .anyRequest().authenticated())
            .build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return email -> userRepository.findByEmail(email)
            .map(user -> User.withUsername(user.getEmail())
                .password(user.getPassword())
                .roles(user.getRole().name())
                .build()
            )
            .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
        auth.setPasswordEncoder(passwordEncoder());
        auth.setUserDetailsService(userDetailsService());
        return auth;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(List.of("http://localhost:4200"));
        configuration.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(List.of("*"));
        configuration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
