package pl.webc.webcg7spring.product.service;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.webc.webcg7spring.common.exceptions.ConflictException;
import pl.webc.webcg7spring.common.exceptions.NotFoundException;
import pl.webc.webcg7spring.product.data.Product;
import pl.webc.webcg7spring.product.data.ProductRepository;

@RequiredArgsConstructor
@Service // adnotacja tworząca bean
class ProductServiceImpl implements ProductService {

    private final ProductRepository repository; // lombok wytzrykuje zależność poprzez adnotacje kontstruktora

    @Override
    public List<Product> findAllProducts() {
        return repository.findAll();
    }

    @Override
    public Product findProduct(Integer needleId) { // zwraca tylko NotFoundException albo Product
        return repository.findById(needleId)
            .orElseThrow(() -> new NotFoundException("product with id " + needleId));
    }

    @Override
    public Product saveProduct(Product product) {
        return repository.save(product);
    }

    @Override
    public void deleteProduct(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public void checkConflict(String name) {
        boolean isConflict = repository.existsByName(name);
        if (isConflict) {
            throw new ConflictException("product with name: " + name + " already exists");
        }
    }

    @Override
    public void checkConflict(String name, Integer id) {
        boolean isConflict = repository.existsByNameAndIdNot(name, id);
        if (isConflict) {
            throw new ConflictException("product with name: " + name + " already exists");
        }
    }
}
