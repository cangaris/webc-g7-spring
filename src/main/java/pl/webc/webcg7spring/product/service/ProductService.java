package pl.webc.webcg7spring.product.service;

import java.util.List;
import pl.webc.webcg7spring.product.data.Product;

public interface ProductService {

    List<Product> findAllProducts();

    Product findProduct(Integer needleId);

    Product saveProduct(Product product);

    void deleteProduct(Integer id);

    void checkConflict(String name);

    void checkConflict(String name, Integer id);
}
