package pl.webc.webcg7spring.product.controller;

import java.util.List;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;
import pl.webc.webcg7spring.category.data.Category;
import pl.webc.webcg7spring.common.dtos.ImageDto;
import pl.webc.webcg7spring.common.dtos.ImageSaveDto;
import pl.webc.webcg7spring.image.data.Image;
import pl.webc.webcg7spring.product.data.Product;

@UtilityClass // wszystkie metody statyczne w tej klasie (automatycznie dopisuje static)
class Mappers {

    ProductDto map(Product product) {
        List<ProductDto.CategoryDto> categories = product.getCategories().stream()
            .map(category -> map(category))
            .collect(Collectors.toList());
        List<ImageDto> images = product.getImages().stream()
            .map(imageSaveDto -> map(imageSaveDto))
            .collect(Collectors.toList());
        return ProductDto.builder()
            .id(product.getId())
            .name(product.getName())
            .content(product.getContent())
            .price(product.getPrice())
            .categories(categories)
            .images(images)
            .build();
    }

    Product map(ProductInsertDto productInsertDto) {
        List<Image> images = productInsertDto.getImages().stream()
            .map(imageSaveDto -> map(imageSaveDto))
            .collect(Collectors.toList());
        return Product.builder()
            .name(productInsertDto.getName())
            .content(productInsertDto.getContent())
            .price(productInsertDto.getPrice())
            .images(images)
            .build();
    }

    Image map(ImageSaveDto imageSaveDto) {
        return Image.builder()
            .uri(imageSaveDto.uri())
            .build();
    }

    ImageDto map(Image image) {
        return ImageDto.builder()
            .id(image.getId())
            .uri(image.getUri())
            .build();
    }

    void map(Product product, ProductUpdateDto productUpdateDto) {
        product.setName(productUpdateDto.getName());
        product.setPrice(productUpdateDto.getPrice());
        product.setContent(productUpdateDto.getContent());

        List<Image> imagesToUpdate = productUpdateDto.getImages().stream()
            .map(imageSaveDto -> map(imageSaveDto))
            .collect(Collectors.toList());
        List<Image> images = product.getImages();
        images.clear();
        images.addAll(imagesToUpdate);
    }

    ProductDto.CategoryDto map(Category category) {
        return ProductDto.CategoryDto.builder()
            .id(category.getId())
            .name(category.getName())
            .build();
    }
}
