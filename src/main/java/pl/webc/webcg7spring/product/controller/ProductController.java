package pl.webc.webcg7spring.product.controller;

import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.webc.webcg7spring.common.dtos.DatabaseIdDto;
import pl.webc.webcg7spring.product.data.Product;
import pl.webc.webcg7spring.product.service.ProductService;

@RequiredArgsConstructor
// RequiredArgsConstructor - generuje tylko dla pól final
// tu jest konstruktor odpowiedzialny za IoC / DI
@RestController
class ProductController {

    private final ProductService service; // DI -> ProductServiceImpl (zawołanie beana pośrednio po jego interface)

    @GetMapping("/product") // [GET] /product
    public List<ProductDto> getProducts() {
        return service.findAllProducts().stream()
            .map(product -> Mappers.map(product))
            .toList();
    }

    @GetMapping("/product/{id}") // [GET] /product/{id}
    public ProductDto getProduct(@PathVariable Integer id) {
        Product product = service.findProduct(id);
        return Mappers.map(product);
    }

    @PostMapping("/product") // [POST] /product
    public DatabaseIdDto insertProduct(@RequestBody @Valid ProductInsertDto productInsert) {
        // nazwa bez powtórzeń
        service.checkConflict(productInsert.getName());
        Product product = Mappers.map(productInsert);
        product = service.saveProduct(product);
        return new DatabaseIdDto(product.getId()); // SQL insert bo ID jest NULL
        // ID nie bedzie null jak by moglo to wynikac z linijki 50! bo baza danych wpisze tu prawdziwy ID
    }

    @PutMapping("/product/{id}") // [PUT] /product/{id}
    public void updateProduct(@PathVariable Integer id, @RequestBody @Valid ProductUpdateDto productUpdate) {
        service.checkConflict(productUpdate.getName(), id);
        Product product = service.findProduct(id);
        Mappers.map(product, productUpdate);
        service.saveProduct(product); // update SQL bo ID już jest ustawione w encji
    }

    @DeleteMapping("/product/{id}") // [DELETE] /product/{id}
    public void deleteProduct(@PathVariable Integer id) {
        Product product = service.findProduct(id);
        service.deleteProduct(product.getId());
    }
}
