package pl.webc.webcg7spring.product.controller;

import jakarta.validation.Valid;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import pl.webc.webcg7spring.common.dtos.ImageSaveDto;

@Getter
@Setter
class ProductInsertDto {
    @NotBlank(message = "Name cannot be blank")
    @Size(min = 5, max = 255, message = "Name must have 5-255 chars")
    private String name;

    @NotBlank(message = "Content cannot be blank")
    @Size(min = 5, max = 65_535, message = "Content must have 5-65535 chars")
    private String content;

    @NotNull(message = "Price cannot be null")
    @DecimalMin(value = "1.00", message = "Price must be greater than 1 PLN")
    @DecimalMax(value = "1000000.00", message = "Price must be less than 1 million PLN")
    private BigDecimal price;

    private List<@Valid ImageSaveDto> images;
}
