package pl.webc.webcg7spring.product.controller;

import java.math.BigDecimal;
import java.util.List;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.webc.webcg7spring.common.dtos.ImageDto;

@EqualsAndHashCode
@ToString
@Builder
@Getter
@Setter
class ProductDto {
    private Integer id;
    private String name;
    private String content;
    private BigDecimal price;
    private List<CategoryDto> categories;
    private List<ImageDto> images;

    @Getter
    @Setter
    @Builder
    static class CategoryDto {
        private Integer id;
        private String name;
    }
}
