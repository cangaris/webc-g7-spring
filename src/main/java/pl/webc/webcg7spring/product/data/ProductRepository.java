package pl.webc.webcg7spring.product.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    // Optional<JakisTyp> findByJAKIEŚ_POLE(TypTegoPola jakasNazwa)
    // List<JakisTyp> findAllByJAKIEŚ_POLE(TypTegoPola jakasNazwa)
    // boolean existsByJAKIEŚ_POLE(TypTegoPola jakasNazwa)

    // (JPQL) Java Persistence Query Language, (HQL) Hibernate Query Language
    // @Query("select count(p) > 0 from Product p where p.name = :n")
    // @Param("n")
    boolean existsByName(String name);

    // @Query("select count(p) > 0 from Product p where p.name = ?1 and p.id <> ?2")
    boolean existsByNameAndIdNot(String name, Integer id);

    // @Query("select p from Product p where p.name = ?1")
    // Optional<Product> findByName(String name);

    // @Query("select p from Product p where p.content like %?1%")
    // List<Product> findAllByContentFragment(String fragment);
}
