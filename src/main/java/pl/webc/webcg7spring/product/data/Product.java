package pl.webc.webcg7spring.product.data;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.webc.webcg7spring.category.data.Category;
import pl.webc.webcg7spring.image.data.Image;

@Builder
@AllArgsConstructor // tworzy konstruktor ze wszystkimi argumentami
@NoArgsConstructor // tworzy konstruktor bez argumentów - jest potrzebny dla Hibernate
@Getter // tworzy automatcznie gettery
@Setter // tworzy automatycznie settery
@Entity // encja czyli model tabeli w bazie danych
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // zarządza sekwencją klucza podstawowego
    private Integer id;

    @Column(length = 200, nullable = false, unique = true) // domyslny rozmiar 255
    private String name;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String content;

    @Column(nullable = false)
    private BigDecimal price;

    @ManyToMany(mappedBy = "products")
    private List<Category> categories;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "product_id")
    private List<Image> images;
}
