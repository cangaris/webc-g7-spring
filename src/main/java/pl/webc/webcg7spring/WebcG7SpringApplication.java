package pl.webc.webcg7spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebcG7SpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebcG7SpringApplication.class, args);
    }
}
